package com.csgostats.demo.parser.bitstream;

/**
 * @author Michael
 */
public interface BitStream {
    byte readByte();

    byte[] readBytes(int numBytes);

    int readInt(int numBits);

    String readCString(int stringLength);

    float readFloat();

    void beginChunk(int i);

    boolean isChunkFinished();

    /**
     * Read 1-5 bytes in order to extract a 32-bit unsigned value from the
     * stream. 7 data bits are extracted from each byte with the 8th bit used
     * to indicate whether the loop should continue.
     * This allows variable size numbers to be stored with tolerable
     * efficiency. Numbers sizes that can be stored for various numbers of
     * encoded bits are:
     * 8-bits: 0-127
     * 16-bits: 128-16383
     * 24-bits: 16384-2097151
     * 32-bits: 2097152-268435455
     * 40-bits: 268435456-0xFFFFFFFF
     */
    int readVarInt();

    int readBitInt();

    boolean readBit();

    void endChunk();

    String readString(int length, boolean breakOnNewline);
}
