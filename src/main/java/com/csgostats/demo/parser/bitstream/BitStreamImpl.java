package com.csgostats.demo.parser.bitstream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Stack;

/**
 * @author Michael
 */
public class BitStreamImpl implements BitStream {
    private static final Logger LOGGER = LoggerFactory.getLogger(BitStreamImpl.class);
    private static final int SLED = 4;
    private static final int BUFFER_SIZE = 2048 + SLED;

    // MSB masks (protobuf varint end signal)
    private static final int MSB_1 = 0x00000080;
    private static final int MSB_2 = 0x00008000;
    private static final int MSB_3 = 0x00800000;
    private static final int MSB_4 = 0x80000000;

    // byte masks (except MSB)
    private static final int MSK_1 = 0x0000007F;
    private static final int MSK_2 = 0x00007F00;
    private static final int MSK_3 = 0x007F0000;
    private static final int MSK_4 = 0x7F000000;

    private int offset = 0;
    private InputStream underlyingStream;
    private final byte[] buffer = new byte[BUFFER_SIZE];

    private int bitsInBuffer = 0;

    private final Stack<Long> chunkTargets = new Stack<>();
    private long lazyGlobalPosition = 0;

    public BitStreamImpl(InputStream inputStream) {
        this.underlyingStream = inputStream;
        this.refillBuffer();
        this.offset = SLED * 8;
    }

    private void refillBuffer() {
        System.arraycopy(buffer, (bitsInBuffer / 8), buffer, 0, SLED);

        offset -= bitsInBuffer;
        lazyGlobalPosition += bitsInBuffer;

        int currentOffset;
        int thisTime = 0;
        for (currentOffset = 0; (currentOffset < 4) && (thisTime >= 0); currentOffset += thisTime) {
            try {
                thisTime = underlyingStream.read(buffer, SLED + currentOffset, BUFFER_SIZE - SLED - currentOffset);
            } catch (IOException readEx) {
                LOGGER.error("Error reading from underlying stream. Closing the stream.");
                try {
                    underlyingStream.close();
                } catch (IOException closeEx) {
                    LOGGER.error("Error closing underlying stream.", closeEx);
                }
                throw new RuntimeException(readEx);
            }
        }

        bitsInBuffer = 8 * currentOffset;

        if (thisTime == -1) {
            // end of stream, so we can consume the sled now
            bitsInBuffer += SLED * 8;
        }
    }

    private void advance(int howMuch) {
        offset += howMuch;
        while (offset >= bitsInBuffer) {
            refillBuffer();
        }
    }

    @Override
    public boolean readBit() {
        boolean bit = (buffer[offset/8] & (1 << (offset & 7))) != 0;
        advance(1);
        return bit;
    }

    @Override
    public byte[] readBytes(int numBytes) {
        byte[] bytes = new byte[numBytes];
        for (int i = 0; i < numBytes; i++) {
            bytes[i] = readByte();
        }
        return bytes;
    }

    @Override
    public byte readByte() {
        return readByte(8);
    }

    private byte readByte(int numBits) {
        return (byte) readInt(numBits);
    }

    @Override
    public int readInt(int numBits) {
        int result = peekInt(numBits);
        advance(numBits);
        return result;
    }

    @Override
    public float readFloat() {
        return ByteBuffer.wrap(readBytes(4)).order(ByteOrder.LITTLE_ENDIAN).getFloat();
    }

    private int peekInt(int numBits) {
        return peekInt(numBits, false);
    }

    private int peekInt(int numBits, boolean mayOverflow) {
        byte[] bytes = new byte[8];
        System.arraycopy(buffer, (offset / 8) & ~3, bytes, 0, 8);
        long longFromByteArray = BitStreamUtil.getLongFromLittleEndianByteArray(bytes);
        return BitStreamUtil.extractIntFromBitsOfLong(longFromByteArray, ((8 * 8) - (offset % (8 * 4)) - numBits), numBits);
    }

    @Override
    public String readCString(int stringLength) {
        return new String(readBytes(stringLength)).split("\0", 2)[0];
    }

    @Override
    public int readVarInt() {
        int buf = peekInt(32, true);

        int result = buf & MSK_1;
        if ((buf & MSB_1) != 0) {
            result |= (buf & MSK_2) >> 1;
            if ((buf & MSB_2) != 0) {
                result |= (buf & MSK_3) >> 2;
                if ((buf & MSB_3) != 0) {
                    result |= (buf & MSK_4) >> 3;
                    if ((buf & MSB_4) != 0) {
                        // value too large, fall back to slower implementation.
                        // this usually doesn't happen, I swear
                        // TODO: Log warning
                        return BitStreamUtil.readVarInt(this);
                    } else {
                        advance(4 * 8);
                    }
                } else {
                    advance(3 * 8);
                }
            } else {
                advance(2 * 8);
            }
        } else {
            advance(8);
        }

        return result;
    }

    @Override
    public int readBitInt() {
        int ret = this.readInt(6);
        switch (ret & (16 | 32)) {
            case 16:
                ret = (ret & 15) | (this.readInt(4) << 4);
                break;
            case 32:
                ret = (ret & 15) | (this.readInt(8) << 4);
                break;
            case 48:
                ret = (ret & 15) | (this.readInt(32 - 4) << 4);
                break;
        }
        return ret;
    }

    private long getActualGlobalPosition() {
        return lazyGlobalPosition + offset;
    }

    @Override
    public void beginChunk(int chunkLength) {
        chunkTargets.push(getActualGlobalPosition() + chunkLength);
    }

    @Override
    public void endChunk() {
        long target = chunkTargets.pop();
        int delta = (int) (target - getActualGlobalPosition());
        if (delta < 0) {
            throw new IllegalStateException("The application read beyond the chunk boundary");
        } else if (delta > 0) {
            // TODO: look into increasing performance here
            advance(delta);
        }
    }

    @Override
    public String readString(int maxLength, boolean breakOnNewline) {
        if (maxLength <= 0) {
            throw new RuntimeException("maxLength must be > 0");
        }

        boolean isMaxLengthTooShort = false;
        int i = 0;
        byte[] bytes = new byte[maxLength];
        while (true) {
            byte val = readByte();
            if (val == 0 || (breakOnNewline && val == '\n')) {
                break;
            }

            if (i < (maxLength - 1)) {
                bytes[i] = val;
            } else {
                isMaxLengthTooShort = true;
            }
            i++;
        }

        if (isMaxLengthTooShort) {
            LOGGER.warn("Attempted to read a String with max length {} but exceeded size. Total potential size = {}", maxLength, i);
        }
        return new String(bytes, 0, i);
   }

    @Override
    public boolean isChunkFinished() {
        return chunkTargets.peek() == getActualGlobalPosition();
    }
}
