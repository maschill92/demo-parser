package com.csgostats.demo.parser.bitstream;

/**
 * @author Michael
 */
public final class BitStreamUtil {
    public static final int MAX_VARIABLE_INT_BYTES = 5;
    private BitStreamUtil() {
    }

    /**
     * Returns a long from a little endian ordered byte array.
     * Note that bytes.length cannot be longer than 8
     */
    public static long getLongFromLittleEndianByteArray(byte[] bytes) {
        if (bytes.length > 8) {
            throw new IllegalArgumentException("Parameter 'bytes' can have length no longer than 8. It is " + bytes.length);
        }
        long ret = 0;
        for (int i = 0; i < bytes.length; i++) {
            ret += ((long) bytes[i] & 0xffL) << (8 * i);
        }
        return ret;
    }

    /**
     * Extracts an 32 bit int from the a 64 bit long.
     * The extraction starts at the bitOffset location of the long and extends numBits long.
     *
     * */
    public static int extractIntFromBitsOfLong(long value, int bitOffset, int numBits) {
        // TODO: Validate bitOffset and numBits.
        long temp = value << bitOffset;
        temp = temp >>> 64 - numBits;
        return (int) temp;
    }

    public static int readVarInt(BitStream stream) {
        byte b = (byte) 128;
        int result = 0;
        for (int count = 0; (b & 0x80) != 0; count++) {
            b = stream.readByte();

            if (count < 4 || ((count == 4) && (((b & 0xF8) == 0) || ((b & 0xF8) == 0xF8)))) {
                result |= (b & ~0x80) << (7 * count);
            } else {
                if (count >= 10) {
                    throw new RuntimeException("More than 10 bytes were attempted to be read for a variable int.");
                } else if ((count == 9) ? (b != 1) : ((b & 0x7F) != 0x7F)) {
                    throw new RuntimeException("More than 32 bits are not supported as data far a variable int.");
                }
            }
        }
        return result;
    }
}
