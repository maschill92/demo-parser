package com.csgostats.demo.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.time.Duration;
import java.time.OffsetDateTime;

/**
 * @author Michael
 */
public class Main {

    private static Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        LOGGER.info("Start");
        OffsetDateTime begin = OffsetDateTime.now();
        InputStream inputStream = new FileInputStream("test.dem");
        DemoParser demoParser = new DemoParser(inputStream);
        demoParser.parseHeader();

        boolean stillParsing;
        do {
            stillParsing = demoParser.parseNextTick();
        } while (stillParsing);
        OffsetDateTime end = OffsetDateTime.now();
        LOGGER.info("End");
        LOGGER.info("Run time: {}", Duration.between(begin, end));
    }
}
