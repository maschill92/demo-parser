package com.csgostats.demo.parser;

import com.csgostats.demo.parser.bitstream.BitStream;
import com.csgostats.demo.parser.bitstream.BitStreamImpl;
import com.csgostats.demo.parser.data.RawData;
import com.csgostats.demo.parser.data.datatable.DataTableParser;
import com.csgostats.demo.parser.data.datatable.DataTableParserImpl;
import com.csgostats.demo.parser.data.packet.PacketParser;
import com.csgostats.demo.parser.data.packet.PacketParserImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

/**
 * @author Michael
 */
public class DemoParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoParser.class);

    private BitStream bitStream;
    private DemoHeader demoHeader;
    private int parserTick = 0;
    private int serverTick = 0;

    private DataTableParser dataTableParser = new DataTableParserImpl();
    private PacketParser packetParser = new PacketParserImpl();

    public DemoParser(InputStream inputStream) {
        this.bitStream = new BitStreamImpl(inputStream);
    }

    public void parseHeader() {
        this.demoHeader = new DemoHeader(bitStream);
        this.demoHeader.validate();
    }

    public boolean parseNextTick() {
        if (demoHeader == null) {
            throw new IllegalStateException("Header is null, called parseHeader first");
        }

        byte cmd = bitStream.readByte();
        DemoCommand command = DemoCommand.fromInt(cmd);
        serverTick = bitStream.readInt(32);
        parserTick++;
        byte playerSlot = bitStream.readByte();

//        LOGGER.debug("{}\t parserTick={}", command, parserTick);
        switch (command) {
            case SYNC_TICK:
                break;
            case STOP:
                return false;
            case CONSOLE_COMMAND:
                RawData.parse(bitStream);
                break;
            case DATA_TABLES:
                dataTableParser.parseDataTable(bitStream);
                break;
            case STRING_TABLES:
                RawData.parse(bitStream);
                break;
            case USER_COMMAND:
                //noinspection unused
                int outgoingSequence = bitStream.readInt(32);
                RawData.parse(bitStream);
                break;
            case SIGN_ON:
            case PACKET:
                packetParser.parsePacket(bitStream);
                break;
            default:
                throw new RuntimeException("Can't handle command " + command);
        }
        return true;
    }
}
