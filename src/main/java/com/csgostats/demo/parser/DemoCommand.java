package com.csgostats.demo.parser;

/**
 * @author Michael
 */
public enum DemoCommand {
    NULL(0), // this is to allow values to be arranged correctly
    SIGN_ON(1),
    PACKET(2),
    SYNC_TICK(3),
    CONSOLE_COMMAND(4),
    USER_COMMAND(5),
    DATA_TABLES(6),
    STOP(7),
    CUSTOM_DATA(8),
    STRING_TABLES(9),
    LAST_COMMAND(STRING_TABLES);

    private final int value;
    private static DemoCommand[] values = null;

    DemoCommand(int value) {
        this.value = value;
    }

    DemoCommand(DemoCommand demoCommand) {
        this.value = demoCommand.getValue();
    }

    public int getValue() {
        return value;
    }

    public static DemoCommand fromInt(int i) {
        if (values == null) {
            // cache for performance.
            values = DemoCommand.values();
        }

        if (i >= values.length) {
            return NULL;
        } else {
            return DemoCommand.values[i];
        }
    }
}
