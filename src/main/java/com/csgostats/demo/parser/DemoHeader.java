package com.csgostats.demo.parser;

import com.csgostats.demo.parser.bitstream.BitStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

/**
 * @author Michael
 */
public class DemoHeader {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoHeader.class);

    private static final String HEADER_ID = "HL2DEMO";
    private static final int DEMO_PROTOCOL = 4;
    private static final String CLIENT_NAME = "GOTV Demo";
    private static final String GAME_DIRECTORY = "csgo";

    private static final int MAX_OSPATH = 260;

    private String headerId;
    private int demoProtocol;
    private int networkProtocol;
    private String serverName;
    private String clientName;
    private String mapName;
    private String gameDirectory;
    private float playbackTime;
    private int playbackTicks;
    private int playbackFrames;
    private int signOnLength;

    public DemoHeader(BitStream stream) {
        headerId = stream.readCString(8);              // HL2DEMO\0
        demoProtocol = stream.readInt(32);              // 4
        networkProtocol = stream.readInt(32);
        serverName = stream.readCString(MAX_OSPATH);
        clientName = stream.readCString(MAX_OSPATH);    // GOTV Demo
        mapName = stream.readCString(MAX_OSPATH);
        gameDirectory = stream.readCString(MAX_OSPATH); // csgo
        playbackTime = stream.readFloat();
        playbackTicks = stream.readInt(32);
        playbackFrames = stream.readInt(32);
        signOnLength = stream.readInt(32);
    }

    public void validate() {
        boolean error = false;
        if (!headerId.equals(HEADER_ID)) {
            LOGGER.error(format("Incorrect Demo Header ID parsed. Found '%s' but expected '%s'.%n", headerId, HEADER_ID));
            error = true;
        }
        if (demoProtocol != DEMO_PROTOCOL) {
            LOGGER.error(format("Incorrect Demo Protocol parsed. Found '%s' but expected '%s'.%n", demoProtocol, DEMO_PROTOCOL));
            error = true;
        }
        if (!clientName.equals(CLIENT_NAME)) {
            LOGGER.error(format("Incorrect Client Name parsed. Found '%s' but expected '%s'.%n", clientName, CLIENT_NAME));
            error = true;
        }
        if (!gameDirectory.equals(GAME_DIRECTORY)) {
            LOGGER.error(format("Incorrect Game Directory parsed. Found '%s' but expected '%s'.%n", gameDirectory, GAME_DIRECTORY));
            error = true;
        }

        if (error) {
            throw new RuntimeException("Error parsing demo header.");
        }
    }

    public String getHeaderId() {
        return headerId;
    }

    public long getDemoProtocol() {
        return demoProtocol;
    }

    public int getNetworkProtocol() {
        return networkProtocol;
    }

    public String getServerName() {
        return serverName;
    }

    public String getClientName() {
        return clientName;
    }

    public String getMapName() {
        return mapName;
    }

    public String getGameDirectory() {
        return gameDirectory;
    }

    public float getPlaybackTime() {
        return playbackTime;
    }

    public int getPlaybackTicks() {
        return playbackTicks;
    }

    public int getPlaybackFrames() {
        return playbackFrames;
    }

    public int getSignOnLength() {
        return signOnLength;
    }
}
