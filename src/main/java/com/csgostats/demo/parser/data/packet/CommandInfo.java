package com.csgostats.demo.parser.data.packet;

import com.csgostats.demo.parser.bitstream.BitStream;

/**
 * @author Michael
 */
public class CommandInfo {

    private static final int MAX_SPLITSCREEN_CLIENTS = 2;

    private Split[] splitArray;

    public CommandInfo(BitStream bitStream) {
        this.splitArray = new Split[MAX_SPLITSCREEN_CLIENTS];
        for (int i = 0; i < MAX_SPLITSCREEN_CLIENTS; i++) {
            this.splitArray[i] = new Split(bitStream);
        }
    }

    public Split[] getSplitArray() {
        return splitArray;
    }
}
