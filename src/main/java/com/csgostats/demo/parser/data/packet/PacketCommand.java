package com.csgostats.demo.parser.data.packet;

/**
 * @author Michael
 */
public enum PacketCommand {
    NET_NOP(0),
    NET_DISCONNECT(1),
    NET_FILE(2),
    NET_SPLIT_SCREEN_USER(3),
    NET_TICK(4),
    NET_STRING_COMMAND(5),
    NET_SET_CON_VAR(6),
    NET_SIGN_ON_STATE(7),

    SVC_SERVER_INFO(8),
    SVC_SEND_TABLE(9),
    SVC_CLASS_INFO(10),
    SVC_SET_PAUSE(11),
    SVC_CREATE_STRING_TABLE(12),
    SVC_UPDATE_STRING_TABLE(13),
    SVC_VOICE_INIT(14),
    SVC_VOICE_DATA(15),
    SVC_PRINT(16),
    SVC_SOUNDS(17),
    SVC_SET_VIEW(18),
    SVC_FIX_ANGLE(19),
    SVC_CROSSHAIR_ANGLE(20),
    SVC_BSP_DECAL(21),
    SVC_SPLIT_SCREEN(22),
    SVC_USER_MESSAGE(23),
    SVC_ENTITY_MESSAGE(24),
    SVC_GAME_EVENT(25),
    SVC_PACKET_ENTITIES(26),
    SVC_TEMP_ENTITIES(27),
    SVC_PREFETCH(28),
    SVC_MENU(29),
    SVC_GAME_EVENT_LIST(30),
    SVC_GET_CVAR_VALUE(31),
    SVC_PAINT_MAP_DATA(33),
    SVC_CMD_KEY_VALUES(34),
    SVC_ENCRYPTED_DATA(35);

    private final int value;

    PacketCommand(int value) {
        this.value = value;
    }

    public static PacketCommand fromInt(int i) {
        for (int j = 0; j < PacketCommand.values().length; j++) {
            if (PacketCommand.values()[j].value == i) {
                return PacketCommand.values()[j];
            }
        }
        throw new RuntimeException("There is not PacketCommand with value " + i);
    }
}
