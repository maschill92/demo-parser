package com.csgostats.demo.parser.data.packet;

import com.csgostats.demo.parser.bitstream.BitStream;

/**
 * @author Michael
 */
public class QAngle {
    private float x;
    private float y;
    private float z;

    public QAngle(BitStream bitStream) {
        x = bitStream.readFloat();
        y = bitStream.readFloat();
        z = bitStream.readFloat();
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }
}
