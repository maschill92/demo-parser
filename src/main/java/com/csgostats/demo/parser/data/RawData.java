package com.csgostats.demo.parser.data;

import com.csgostats.demo.parser.bitstream.BitStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RawData {
    private static final Logger LOGGER = LoggerFactory.getLogger(RawData.class);

    private RawData() {
    }

    public static void parse(BitStream bitStream) {
        int dataSizeBytes = bitStream.readInt(32);
        LOGGER.debug("rawDataSizeBytes={}", dataSizeBytes);

        bitStream.beginChunk(dataSizeBytes * 8);
        bitStream.endChunk();
    }
}
