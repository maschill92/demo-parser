package com.csgostats.demo.parser.data.packet;

public enum UserMessageCommand {
    VGUI_MENU(1),
    GEIGER(2),
    TRAIN(3),
    HUD_TEXT(4),
    SAY_TEXT(5),
    SAY_TEXT_2(6),
    TEXT_MSG(7),
    HUD_MSG(8),
    RESET_HUD(9),
    GAME_TITLE(10),
    SHAKE(12),
    FADE(13),
    RUMBLE(14),
    CLOSE_CAPTION(15),
    CLOSE_CAPTION_DIRECT(16),
    SEND_AUDIO(17),
    RAW_AUDIO(18),
    VOICE_MASK(19),
    REQUEST_STATE(20),
    DAMAGE(21),
    RADIO_TEXT(22),
    HINT_TEXT(23),
    KEY_HINT_TEXT(24),
    PROCESS_SPOTTED_ENTITY_UPDATE(25),
    RELOAD_EFFECT(26),
    ADJUST_MONEY(27),
    UPDATE_TEAM_MONDY(28),
    STOP_SPECTATOR_MODE(29),
    KILL_CAM(30),
    DESIRED_TIMESCALE(31),
    CURRENT_TIMESCALE(32),
    ACHIEVEMENT_EVENT(33),
    MATCH_END_CONDITIONS(34),
    DISCONNECT_TO_LOBBY(35),
    PLAYER_STATS_UPDATE(36),
    DISPLAY_INVENTORY(37),
    WARMUP_HAS_ENDED(38),
    CLIENT_INFO(39),
    CALL_VOTE_FAILED(45),
    VOTE_START(46),
    VOTE_PASS(47),
    VOTE_FAILED(48),
    VOTE_SETUP(49),
    SEND_LAST_KILLER_DAMAGE_TO_CLIENT(51),
    ITEM_PICKUP(53),
    SHOW_MENU(54),
    BAR_TIME(55),
    AMMO_DENIED(56),
    MARK_ACHIEVEMENT(57),
    ITEM_DROP(59),
    GLOW_PROP_TURN_OFF(60);

    private final int value;

    UserMessageCommand(int value) {
        this.value = value;
    }

    public static UserMessageCommand fromInt(int i) {
        for (int j = 0; j < UserMessageCommand.values().length; j++) {
            if (UserMessageCommand.values()[j].value == i) {
                return UserMessageCommand.values()[j];
            }
        }
        throw new RuntimeException("There is not a UserMessageCommand with value " + i);
    }
}
