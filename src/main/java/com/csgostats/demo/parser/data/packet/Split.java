package com.csgostats.demo.parser.data.packet;

import com.csgostats.demo.parser.bitstream.BitStream;

/**
 * @author Michael
 */
public class Split {
    private static final int FDEMO_NORMAL = (0);
    private static final int FDEMO_USE_ORIGIN2 = (1 << 0);
    private static final int FDEMO_USE_ANGLES2 = (1 << 1);
    private static final int FDEMO_NOINTERP = (1 << 2);

    private int flags;

    // original origin/viewangles
    private Vector viewOrigin;
    private QAngle viewAngles;
    private QAngle localViewAngles;

    // Resampled origin/viewangles
    private Vector viewOrigin2;
    private QAngle viewAngles2;
    private QAngle localViewAngles2;

    public Split(BitStream bitStream) {
        this.flags = bitStream.readInt(32);

        viewOrigin = new Vector(bitStream);
        viewAngles = new QAngle(bitStream);
        localViewAngles = new QAngle(bitStream);

        viewOrigin2 = new Vector(bitStream);
        viewAngles2 = new QAngle(bitStream);
        localViewAngles2 = new QAngle(bitStream);
    }

    public int getFlags() {
        return flags;
    }

    public Vector getViewOrigin() {
        return viewOrigin;
    }

    public QAngle getViewAngles() {
        return viewAngles;
    }

    public QAngle getLocalViewAngles() {
        return localViewAngles;
    }

    public Vector getViewOrigin2() {
        return viewOrigin2;
    }

    public QAngle getViewAngles2() {
        return viewAngles2;
    }

    public QAngle getLocalViewAngles2() {
        return localViewAngles2;
    }

    public Vector getMasterViewOrigin() {
        if ((flags & FDEMO_USE_ORIGIN2) != 0) {
            return viewOrigin2;
        }
        return viewOrigin;
    }

    public QAngle getMasterViewAngles() {
        if ((flags & FDEMO_USE_ANGLES2) != 0) {
            return viewAngles2;
        }
        return viewAngles;
    }

    public QAngle getMasterLocalViewAngles() {
        if ((flags & FDEMO_USE_ANGLES2) != 0) {
            return localViewAngles2;
        }
        return localViewAngles;
    }
}
