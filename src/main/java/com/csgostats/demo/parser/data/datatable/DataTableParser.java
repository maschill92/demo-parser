package com.csgostats.demo.parser.data.datatable;

import com.csgostats.demo.parser.bitstream.BitStream;

public interface DataTableParser {
    void parseDataTable(BitStream bitStream);
}
