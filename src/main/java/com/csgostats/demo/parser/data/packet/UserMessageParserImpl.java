package com.csgostats.demo.parser.data.packet;

import com.csgostats.demo.protobuf.Cstrike15UsermessagesPublic.CCSUsrMsg_WarmupHasEnded;
import com.csgostats.demo.protobuf.NetmessagesPublic.CSVCMsg_UserMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserMessageParserImpl implements UserMessageParser {
    private static Logger LOGGER = LoggerFactory.getLogger(UserMessageParserImpl.class);

    @Override
    public void parse(CSVCMsg_UserMessage userMessage) {
        UserMessageCommand userMessageCommand = UserMessageCommand.fromInt(userMessage.getMsgType());
        try {
            switch (userMessageCommand) {
                case WARMUP_HAS_ENDED:
                    CCSUsrMsg_WarmupHasEnded warmupHasEnded = CCSUsrMsg_WarmupHasEnded.parseFrom(userMessage.getMsgData());
                    break;
                default:
                    break;
            }
        } catch (InvalidProtocolBufferException ipbe) {
            LOGGER.trace("Could not parse user message data for {} command type", userMessageCommand);
            throw new RuntimeException(ipbe);
        }
    }
}
