package com.csgostats.demo.parser.data.packet;

import com.csgostats.demo.parser.bitstream.BitStream;

public interface PacketParser {
    void parsePacket(BitStream bitStream);
}
