package com.csgostats.demo.parser.data.datatable;

import com.csgostats.demo.parser.bitstream.BitStream;
import com.csgostats.demo.protobuf.NetmessagesPublic;
import com.google.protobuf.InvalidProtocolBufferException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DataTableParserImpl implements DataTableParser {
    private static final int NET_MAX_PAYLOAD = 262144 - 4;
    private static Logger LOGGER = LoggerFactory.getLogger(DataTableParserImpl.class);

    private List<NetmessagesPublic.CSVCMsg_SendTable> csvcMsg_sendTables = new ArrayList<>();
    private List<ServerClass> serverClasses = new ArrayList<>();

    @Override
    public void parseDataTable(BitStream bitStream) {
        int dataTablePacketSize = bitStream.readInt(32);
        bitStream.beginChunk(dataTablePacketSize * 8);

        while (true) {
            int type = bitStream.readVarInt();
            NetmessagesPublic.SVC_Messages messageType = NetmessagesPublic.SVC_Messages.valueOf(type);
            if (!NetmessagesPublic.SVC_Messages.svc_SendTable.equals(messageType)) {
                throw new RuntimeException("Expected svc_SendTable, but got " + messageType);
            }

            int tableSize = bitStream.readVarInt();
            if (tableSize < 0 || tableSize > NET_MAX_PAYLOAD) {
                // throw something.
                break;
            }

            bitStream.beginChunk(tableSize * 8);
            byte[] dataTableBytes = bitStream.readBytes(tableSize);
            NetmessagesPublic.CSVCMsg_SendTable sendTable;
            try {
                sendTable = NetmessagesPublic.CSVCMsg_SendTable.parseFrom(dataTableBytes);
            } catch (InvalidProtocolBufferException e) {
                LOGGER.error("Error parsing CSVCMsg_SendTable.", e);
                throw new RuntimeException(e);
            }
            csvcMsg_sendTables.add(sendTable);
            log(sendTable);
            bitStream.endChunk();
            if (sendTable.getIsEnd()) {
                break;
            }
        }

        int serverClassCount = bitStream.readInt(16);
        if (serverClassCount <= 0) {
            throw new RuntimeException("ServerClasses count expected to be > 0. Got " + serverClassCount);
        }

        for (int i = 0; i < serverClassCount; i++) {
            ServerClass serverClass = new ServerClass();
            serverClass.setClassId(bitStream.readInt(16));

            if (serverClass.getClassId() >= serverClassCount) {
                throw new RuntimeException("ServerClass with ID greater than total server class count.");
            }

            serverClass.setName(bitStream.readString(256, false));
            serverClass.setDTName(bitStream.readString(256, false));

            for (int j = 0; j < csvcMsg_sendTables.size(); j++) {
                NetmessagesPublic.CSVCMsg_SendTable sendTable = csvcMsg_sendTables.get(j);
                if (serverClass.getDTName().equals(sendTable.getNetTableName())) {
                    serverClass.setDataTable(j);
                    break;
                }
            }

            serverClasses.add(serverClass);

            log(serverClass);
        }

        bitStream.endChunk();
    }

    private void log(ServerClass entry) {
        LOGGER.debug("Class:{}:{}:{}({})", entry.getClassId(), entry.getName(), entry.getDTName(), entry.getDataTable());
    }

    private void log(NetmessagesPublic.CSVCMsg_SendTable sendTable) {
        LOGGER.debug("Table:{}:{}", sendTable.getNetTableName(), sendTable.getPropsCount());
        for (NetmessagesPublic.CSVCMsg_SendTable.sendprop_t sendProp : sendTable.getPropsList()) {
            if (sendProp.getType() == 6) {
                // DPT_DataTable
                LOGGER.debug("Prop:{}:{}:{}:{}{}", sendProp.getType(), sendProp.getFlags(), sendProp.getVarName(), sendProp.getDtName(), (sendProp.getFlags() & 1 << 6) != 0 ? " exclude" : "");
            } else if (sendProp.getType() == 5) {
                // DPT_Array
                LOGGER.debug("Prop:{}:{}:{}[{}]", sendProp.getType(), sendProp.getFlags(), sendProp.getVarName(), sendProp.getNumElements());
            } else {
                LOGGER.debug("Prop:{}:{}:{}:{},{}{}{}", sendProp.getType(), sendProp.getFlags(), sendProp.getVarName(), sendProp.getLowValue(), sendProp.getHighValue(), sendProp.getNumBits(), (sendProp.getFlags() & 1 << 8) != 0 ? " inside array" : "");
            }
        }
    }
}
