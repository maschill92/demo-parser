package com.csgostats.demo.parser.data.datatable;

public class ServerClass {
    private int classId;
    private String name;
    private String dtName;
    private int dataTable;

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDTName() {
        return dtName;
    }

    public void setDTName(String dtName) {
        this.dtName = dtName;
    }

    public int getDataTable() {
        return dataTable;
    }

    public void setDataTable(int dataTable) {
        this.dataTable = dataTable;
    }
}
