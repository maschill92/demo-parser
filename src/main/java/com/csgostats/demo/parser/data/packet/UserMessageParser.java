package com.csgostats.demo.parser.data.packet;

import com.csgostats.demo.protobuf.NetmessagesPublic.CSVCMsg_UserMessage;

public interface UserMessageParser {
    void parse(CSVCMsg_UserMessage userMessage);
}
