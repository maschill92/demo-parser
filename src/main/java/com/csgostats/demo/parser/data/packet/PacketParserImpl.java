package com.csgostats.demo.parser.data.packet;

import com.csgostats.demo.parser.bitstream.BitStream;
import com.csgostats.demo.protobuf.NetmessagesPublic.*;
import com.google.protobuf.InvalidProtocolBufferException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PacketParserImpl implements PacketParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(PacketParserImpl.class);
    private CommandInfo commandInfo;
    private int nSeqNrIn;
    private int nSeqNrOut;

    private UserMessageParser userMessageParser = new UserMessageParserImpl();

    @Override
    public void parsePacket(BitStream bitStream) {
        this.commandInfo = new CommandInfo(bitStream);
        this.nSeqNrIn = bitStream.readInt(32);
        this.nSeqNrOut = bitStream.readInt(32);

        int packetTotalSize = bitStream.readInt(32);
        bitStream.beginChunk(packetTotalSize * 8);
        while (!bitStream.isChunkFinished()) {
            PacketCommand packetCommand = PacketCommand.fromInt(bitStream.readVarInt());
            int length = bitStream.readVarInt(); // how many bytes long is the packet
            bitStream.beginChunk(length * 8);

            try {
                byte[] bytes = bitStream.readBytes(length);
                switch (packetCommand) {
                    case NET_NOP:
                        CNETMsg_NOP cnetMsg_nop = CNETMsg_NOP.parseFrom(bytes);
                        break;
                    case NET_TICK:
                        CNETMsg_Tick cnetMsg_tick = CNETMsg_Tick.parseFrom(bytes);
                        break;
                    case SVC_SERVER_INFO:
                        CSVCMsg_ServerInfo csvcMsg_serverInfo = CSVCMsg_ServerInfo.parseFrom(bytes);
                        break;
                    case SVC_GAME_EVENT_LIST:
                        CSVCMsg_GameEventList csvcMsg_gameEventList = CSVCMsg_GameEventList.parseFrom(bytes);
                        break;
                    case SVC_CREATE_STRING_TABLE:
                        CSVCMsg_CreateStringTable csvcMsg_createStringTable = CSVCMsg_CreateStringTable.parseFrom(bytes);
                        break;
                    case SVC_GAME_EVENT:
                        CSVCMsg_GameEvent csvcMsg_gameEvent = CSVCMsg_GameEvent.parseFrom(bytes);
                        break;
                    case SVC_PACKET_ENTITIES:
                        CSVCMsg_PacketEntities csvcMsg_packetEntities = CSVCMsg_PacketEntities.parseFrom(bytes);
                        break;
                    case SVC_UPDATE_STRING_TABLE:
                        CSVCMsg_UpdateStringTable csvcMsg_updateStringTable = CSVCMsg_UpdateStringTable.parseFrom(bytes);
                        break;
                    case SVC_USER_MESSAGE:
                        CSVCMsg_UserMessage userMessage = CSVCMsg_UserMessage.parseFrom(bytes);
                        userMessageParser.parse(userMessage);
                        break;
                    default:
//                        LOGGER.debug("Ignoring packet command: {}", packetCommand);
                        break;
                }
            } catch (InvalidProtocolBufferException ipbe) {
                LOGGER.trace("Could not parse packet data for {} command type", packetCommand);
                throw new RuntimeException(ipbe);
            }
            bitStream.endChunk();
        }
        bitStream.endChunk();
    }
}
