Classes:
1. CDemoFileDump:
2. CDemoFile:

Application Flow:
- main
    - DemoFileDump::Open(fileName) (demoinfogo.cpp:130)
        - CDemoFile::Open(fileName)
            Opens file, parses header based on size_of(m_DemoHeader),
            ensures that rest of file is big enough to parse,
            and validates header values

    - DemoFileDump.DoDump() (demofiledump.cpp:1658)
        - Loops while (!demofinished)
        - CDemoFile::ReadCmdHeader() (demofile.cpp:58)
            cmd <- read next unsigned char from buffer
            tick <- read next int32 from buffer
            playerSlot <- read next unsigned char from buffer
        -
